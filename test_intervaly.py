# -*- coding: utf-8 -*-

import pytest
from intervaly import Interval

def test_SetOd():
    d = Interval()

    d.SetOd("12:53")
    assert d.od == 12*60 + 53

    d.SetOd("22c53  ")
    assert d.od == 22*60 + 53 
    
    d.SetOd("  13:13")
    assert d.od == 13*60 + 13

    d.SetOd("1p:53")
    assert d.od == None

    d.SetOd("12:5f      ")
    assert d.od == None
    
    d.SetOd("2:55")
    assert d.od == 2*60 + 55

    
def test_SetDo():
    d = Interval()

    d.SetDo("14:15")
    assert d.do == 14*60 + 15

    d.SetDo("14g15")
    assert d.do == 14*60 + 15
    
    d.SetDo("d4g15")
    assert d.do == None

    d.SetDo("14gh5")
    assert d.do == None
    
    d.SetDo("2:55")
    assert d.do == 2*60 + 55

    d.SetOd("08:00")
    d.SetDo("07:59")
    assert d.od == 8*60 + 0
    assert d.do == None


def test_Days():
    d = Interval()

    d.SetDays({1,2,3,4,5})
    assert d.days == {1,2,3,4,5}

def test_Teplota():
    d = Interval()

    d.SetTemp("23,6")
    assert d.temp == 236
    
    d.SetTemp("28.9")
    assert d.temp == 289
    
    err = d.SetTemp("")
    assert err == 1
    assert d.temp == 180
    
    err = d.SetTemp("8.1")
    assert err == 2
    assert d.temp == 180
    
    err = d.SetTemp("37.9")
    assert err == 2
    assert d.temp == 180
    
    err = d.SetTemp("d8.1")
    assert err == 3
    assert d.temp == 180
    
def test_Rooms():
    d = Interval()

    d.SetRooms({1,2,3,4,5})
    assert d.rooms == {1,2,3,4,5}  

