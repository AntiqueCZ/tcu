# -*- coding: utf-8 -*-

from terminal import Term
from intervaly import Interval
from datetime import datetime

class House():
    # seznam terminálů
    dum = []
    
    # seznam intervalů
    interval = []
    
    # slovník adres/ID a pojmenování instalovaných místností/pokojů
    #rooms = set()
    mistn = dict()
    
    # režim dovolené -> hlídá se pouze defaultní teplota 
    dovolena = False
    
    # status napájení 12V větve ON/OFF
    napajeni = True
    
    # kotel sepnut / nesepnut
    #  1  : relé sepnuto => topí
    #  0  : relé nesepnuto => netopí
    # >10 : čas (v sec), kdy byl sepnut první ventil (od epochy)
    #       prodleva od otevření prvního ventilu ke spuštění kotle
    #       je nastavena na 60 sec.
    kotel = 0
    
    # hytereze je nastavena na 0,5°C (v 1/10°C)
    HYSTEREZE = 5
    
   
    def __init__(self):
        
        # načtení inicializace ze souboru
        with open("config/house.csv", "r", encoding="utf-8") as f:
            
            for line in f.readlines():
                
                # vkaždém řádku je uloženo pojmenování místnosti a adresa terminálu
                add, jm, temp = line.strip().split(";")
                
                # vytvoření instance terminálu a její přidání do seznamu
                self.dum.append(Term(umisteni = jm, adresa = int(add), tempDef = int(temp)))
                
                # přidání adresy do množiny
                #self.rooms.add(int(add))
                self.mistn[int(add)] = jm
                
        with open("config/intervaly.csv", "r", encoding="utf-8") as f:
            
            for line in f.readlines():
                
                # v každém řádku je uložen jeden interval
                id, od, do, rooms, days, temp = line.strip().split(";")
                
                # vytvoření instance intervalu a její přidání do seznamu
                buff = Interval()
                
                buff.id = int(id)
                buff.od = int(od)
                buff.do = int(do)
                buff.SetDays(days.split(' '))
                buff.SetRooms(rooms.split(' '))
                buff.temp = int(temp)
                
                self.interval.append(buff)
                
                
    def UlozitIntervaly(self):
        ''' Uložení aktuálního stavu intervalů do souboru
            intervaly.csv
            
            pořadí položek:
            
            ID; od; do; rooms; days; temp
            
        '''
        
        print("Jsem v ukladaci rutině...")
        
        #otevření souboru pro zápis
        with open("config/intervaly.csv", "w", encoding="utf-8") as file:
            
            # při uložení dojde k přeindexování ID intervalů
            index = 0
            
            print(str(len(self.interval)))
            
            for i in self.interval:
        
                # množinu místností (resp jejich ID/RS485 adres) musím převést na řetězec
                # kvůli převodu na seznam (class 'list') musí být mezi položkami vložena mezera
                r=""
                for b in i.rooms:
                    r += str(b) + ' '
                
                # množinu dní musím převést na řetězec
                # kvůli převodu na seznam (class 'list') musí být mezi položkami vložena mezera
                d=""
                for b in i.days:
                    d += str(b) + ' '
                
                # sestavení řádku pro zápis do souboru
                buff = "{};{};{};{};{};{}\n".format(index,i.od,i.do,r.strip(),d.strip(),i.temp)
                
                # zápis řádku do souboru
                file.write(buff)
                
                index += 1
                
        
        return 0
        
                
    def Ulozit_konfiguraci(self):
        ''' uložení aktuální konfigurace do souboru
        
        '''
        
        with open("config/house.csv", "w", encoding="utf-8") as f:
            
            for b in self.dum:
                
                buff = "{};{};{}\n".format(b.addr,b.room,b.tempDef)
                
                f.write(buff)
                
                
    def AddNewInt(self):
        ''' přidání prázdného intervalu do seznamu
        
        '''
        buff = Interval()
        
        # počítání od nuly, proto nepřičítám jedničku!!!
        x = len(self.interval)
        
        buff.id = x
        buff.od = 360
        buff.do = 480
        buff.SetDays('')
        buff.SetRooms('')
        buff.temp = 200
        
        self.interval.append(buff)
        
        return x
    
    
    def Pozadovana(self,term_ID,**kwargs):
        ''' algoritmus pro vyčtení požadované teploty
        
        return int<1/10°C>
               -1 pro nastavení defaultní teploty 
                  (žádný interval nebyl použit)
        
        '''        
        
        # získání aktuálního času (v minutách) a dne v týdnu
        dt = datetime.now()
        akt_cas = 60*int(dt.strftime("%H")) + int(dt.strftime("%M"))
        akt_den = dt.isoweekday()
        
        # debug nastavení hodnot času a dne pro testy
        try:
           if kwargs["debug"] == True:
               akt_cas = kwargs["cas"]
               akt_den = kwargs["den"]
        except:
            pass
        
        buff = 0
        
        # nutno projít všechny intervaly
        for i in self.interval:
            
            # nejprve test místnosti
            if term_ID in i.rooms:
                
                # test dne v týdnu
                if akt_den in i.days:
                    
                    # test času od
                    if akt_cas > i.od:
                        
                        # test času do 
                        if akt_cas < i.do:
                            
                            # nejvyšší vyhrává
                            if i.temp > buff:
                                buff = i.temp

        if buff == 0: 
            return -1
        
        return buff
    
