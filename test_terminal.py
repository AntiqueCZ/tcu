# -*- coding: utf-8 -*-

import pytest
from terminal import Term

def test_Aktualni():
    t = Term("test_room",0)

    t.tempAct = 0
    assert t.Aktualni() == "0.0°C"

    t.tempAct = 100
    assert t.Aktualni() == "10.0°C"

    t.tempAct = 253
    assert t.Aktualni() == "25.3°C"

def test_Pozadovana():
    t = Term("test_room",0)

    t.tempReq = 0
    assert t.Pozadovana() == "0.0°C"

    t.tempReq = 100
    assert t.Pozadovana() == "10.0°C"

    t.tempReq = 253
    assert t.Pozadovana() == "25.3°C"

def test_Zbyvajici():
    t = Term("test_room",0)
    
    t.bypass = None
    assert t.Zbyvajici() == ""

    t.bypass = 0
    assert t.Zbyvajici() == "konec"

    t.bypass = 59
    assert t.Zbyvajici() == "59 min."

    t.bypass = 65
    assert t.Zbyvajici() == "1:05 hod."

    t.bypass = 157
    assert t.Zbyvajici() == "2:37 hod."

def test_Vlhkost():
    t = Term("test_room",0)

    t.humidity = 9
    assert t.Vlhkost() == "err"

    t.humidity = 991
    assert t.Vlhkost() == "err"

    t.humidity = 493
    assert t.Vlhkost() == "49.3 %"
    
def test_Set_bpsTemp():
    t = Term("test_room",0)
    
    t.Set_bpsTemp("")
    assert t.bypass == 0
    
    t.Set_bpsTemp("25,6")
    assert t.bpsTemp == 256
    
    t.Set_bpsTemp("12.8")
    assert t.bpsTemp == 128
    
    t.bpsTemp = 155    
    err = t.Set_bpsTemp("fff")
    assert t.bpsTemp == 155 and err == 2

    t.bpsTemp = 177    
    err = t.Set_bpsTemp("36,2")
    assert t.bpsTemp == 177 and err == 1
    
def test_Set_bypass():
    t = Term("test",0)
    
    t.Set_bypass("")
    assert t.bypass == 0
    
    t.Set_bypass("5")
    assert t.bypass == 5*60
    
    t.Set_bypass("5,5")
    assert t.bypass == 5*60 + 5*6

    t.Set_bypass("2.9")
    assert t.bypass == 2*60 + 9*6
    
    t.Set_bypass("2:56")
    assert t.bypass == 2*60 + 56
    
    err = t.Set_bypass("2:88")
    # kvůli chybě zůstává předchozí hodnota
    assert t.bypass == 2*60 + 56
    assert err == 2
    
    err = t.Set_bypass("2.v")
    # kvůli chybě zůstává předchozí hodnota
    assert t.bypass == 2*60 + 56
    assert err == 1
    
    err = t.Set_bypass("d.5")
    # kvůli chybě zůstává předchozí hodnota
    assert t.bypass == 2*60 + 56
    assert err == 1
    
    err = t.Set_bypass("5d.5")
    # kvůli chybě zůstává předchozí hodnota
    assert t.bypass == 2*60 + 56
    assert err == 1

    err = t.Set_bypass("65fd")
    # kvůli chybě zůstává předchozí hodnota
    assert t.bypass == 2*60 + 56
    assert err == 4
    
    t.Set_bypass("0,3")
    assert t.bypass == 3*6
    
def test_Get_BpsTime():
    t = Term("test",0)
    
    t.Set_bypass("2,5")
    assert t.Get_BpsTime() == "02:30"
    
    t.Set_bypass("3")
    assert t.Get_BpsTime() == "03:00"
    
    t.Set_bypass("0,5")
    assert t.Get_BpsTime() == "00:30"    
    
    t.Set_bypass("0,3")
    assert t.Get_BpsTime() == "00:18"
    
def test_Get_BpsTemp():
    t = Term("test",0)
    
    t.Set_bpsTemp("25,6")
    assert t.Get_BpsTemp() == "25.6"
    
    t.Set_bpsTemp("12")
    assert t.Get_BpsTemp() == "12.0"
    
def test_Set_tempDef():
    t = Term("test_room",0)
    
    t.Set_tempDef("")
    assert t.tempDef == 150
    
    t.Set_tempDef("25,6")
    assert t.tempDef == 256
    
    t.Set_tempDef("12.8")
    assert t.tempDef == 128
    
    t.Set_tempDef("22")
    assert t.tempDef == 220   
    
    t.tempDef = 155    
    err = t.Set_tempDef("fff")
    assert t.tempDef == 155 and err == 2

    t.tempDef = 177    
    err = t.Set_tempDef("36,2")
    assert t.tempDef == 177 and err == 1
    
def test_Defaultni():
    t = Term("test_room",0)

    t.tempDef = 0
    assert t.Defaultni() == "err"

    t.tempDef = 100
    assert t.Defaultni() == "10.0°C"

    t.tempDef = 253
    assert t.Defaultni() == "25.3°C"
    