# -*- coding: utf-8 -*-

import re
from datetime import datetime

class Term:
    ''' třída pro data z terminálu
    
    room     - pojmenování umístění  
    addr     - adresace na sběrnici RS485 (0 ÷ 7) / ID 
    tempAct  - aktuální teplota (250 => 25,0°C)  
    tempReq  - požadovaná teplota  (250 => 25,0°C)  
    tempDef  - defaultní teplota místnosti (250 => 25,0°C)  
    humidity - aktuální vlhkost (435 => 43,5%)  
    bypass   - konec bypass režimu, v minutách od počátku epochy
    bpsTemp  - teplota pro bypass
    ventil   - topí / netopí -> triak sepnut/nesepnut
     

    '''
        
    def  __init__(self, umisteni="", adresa=0, tempDef=150, alive=False):
        self.room = umisteni
        self.addr = adresa
        self.tempAct = 0
        self.tempReq = 0
        self.humidity = 0
        self.bypass  = 0
        self.bpsTemp = 0
        self.alive = alive
        self.aliveDelay = 0
        self.tempDef = tempDef
        self.ventil = False
        

    def Aktualni(self):
        if self.tempAct == 0:
            return "0.0°C"

        return "{:.1f}°C".format(self.tempAct/10)

    def Pozadovana(self):
        if self.tempReq == 0:
            return "0.0°C"

        return "{:.1f}°C".format(self.tempReq/10)

    def Zbyvajici(self):
        # // je celočíselné dělení
        # %  je zbytek po dělění
        if self.bypass == 0:
            return ""
        
        if self.bypass == None:
            return ""

        x = self.bypass - int(datetime.timestamp(datetime.now()))//60
        
        if x < 60:
            return "{} min.".format(x)

        return "{}:{:0>2} hod.".format(x//60,x%60)

    def Defaultni(self):
        
        if self.tempDef < 50:
            return "err"
        
        return "{:.1f}°C".format(self.tempDef/10)

    def Vlhkost(self):
        if self.humidity<10 or self.humidity>990:
            return "err"
        return "{:.1f} %".format(self.humidity/10)

    def Set_bpsTemp(self,bps_temp=""):
        
        buff = bps_temp.strip()
        
        # prázdný řetězec - předpoklad požadavku konce bypassu
        if len(buff) == 0:
            self.bypass = 0
            return 0

        try:
            # náhrada čárky za tečku -> převod na float a vynásobení 10 -> převod na int
            x = int(float(buff.replace(',','.'))*10)

            if 100 < x < 350:
                self.bpsTemp = x
            else:
                print("Chyba: požadovaná teplota je mimo povolený rozsah")
                return 1
            
        except ValueError:
            print("Chyba: nečíselné zadání teploty")
            return 2
        
    def Set_tempDef(self,temp_def=""):
        
        buff = temp_def.strip()
        
        # prázdný řetězec - předpoklad defaulní teploty 15°C
        if len(buff) == 0:
            self.tempDef = 150
            return 0

        try:
            # náhrada čárky za tečku -> převod na float a vynásobení 10 -> převod na int
            x = int(float(buff.replace(',','.'))*10)

            if 50 < x < 350:
                self.tempDef = x
            else:
                print("Chyba: požadovaná teplota je mimo povolený rozsah")
                return 1
            
        except ValueError:
            print("Chyba: nečíselné zadání teploty")
            return 2        
        
    def Set_bypass(self,bps_time=""):
        
        buff = bps_time.strip()
        
        # aktuální čas od epochy v minutách
        epo = int(datetime.timestamp(datetime.now()))//60
        
        # prázdný řetězec - předpoklad požadavku konce bypassu
        if len(buff) == 0:
            self.bypass = 0
            return 0
        
        # očekávám zadání buď v celých hodinách,
        # nebo v hodinách a minutách, nebo části hodinách
        # 5   0:10    2:30    1,5  2.8
        # náhrada čárky za tečku -> převod na float a vynásobení 10 -> převod na int   
        buff = buff.replace(',','.')
             
        try:
            r = re.match(r"\d{1,3}",buff)
            if r[0] == buff:
                x = int(buff)*60

                if 0 < x:
                    self.bypass = x + epo
                    return 0
                else:
                    print("Chyba: požadovaný čas bypassu je mimo povolený rozsah")
                    return 3
            
        except TypeError:
            print("Chyba: špatné zadání časového rámce")
            return 1
        
        if '.' in buff:
            h,m = buff.split('.')
            
            try:
                hod = int(h)
                # použiji pouze první cifru za desetinnou tečkou
                min = int(m[0]) * 6
                
                self.bypass = hod * 60 + min + epo
                return 0
                
            except ValueError:
                print("Chyba: zadání času není číselné")
                return 1
        
        if ':' in buff:
            h,m = buff.split(':')
            
            try:
                hod = int(h)
                min = int(m)
                if min > 59:
                    print("Chyba: chybné zadání časového údaje")
                    return 2
                self.bypass = hod * 60 + min + epo
                return 0
                
            except ValueError:
                print("Chyba: zadání času není číselné")
                return 1
        
        # žádný očekávaný tvar zadání
        return 4

    def Get_BpsTime(self):
        # // je celočíselné dělení
        # %  je zbytek po dělění
        
        if (self.bypass == 0) or (self.bypass == None):
            return "00:00"
        
        x = self.bypass - int(datetime.timestamp(datetime.now()))//60
        
        return "{:0>2}:{:0>2}".format(x//60,x%60)

    def Get_BpsTemp(self):
        
        return "{:.1f}".format(self.bpsTemp/10)

