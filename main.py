# -*- coding: utf-8 -*-

# importy knihoven
from flask import Flask, render_template, request
from gpiozero import LED
from time import sleep
from datetime import datetime
import ntplib
from os import system
import serial
import threading
import re


ser_port = "/dev/ttyS0"
ser_baud = "57600"
ser_tout = 10.0          # sec

dir  = LED(18)   # definování pinu pro řízení směru toku dat na sběrnici
V12V = LED(8)    # definice pinu pro ovládání napájení 12V větve
V05V = LED(25)   # definice pinu pro ovládání napájení 5V větve - NEPOUŽÍVAT!!!

RELE1 = LED(23)
RELE2 = LED(24)

triak = [
    LED(12),    # SSR 01
    LED(13),    # SSR 02
    LED(19),    # SSR 03
    LED(16),    # SSR 04
    LED(26),    # SSR 05
    LED(20),    # SSR 06
    LED(21),    # SSR 07
    LED(6)      # SSR 08
]

# # test triaků
# while 1:
#     for i in range(8):
#         triak[i].on()
#         sleep(1)
#         triak[i].off()
        


NTP_hour = 0   # hodina, kdy byla naposled aktualizována časová značka z NTP serveru
NTP_sever1 = 'tik.cesnet.cz'
NTP_sever2 = 'tak.cesnet.cz'
NTP_sever3 = 'pool.ntp.org'
NTP_flag   = False


# import vlasní třídy pro uložení dat z terminálů
from house import House

# ---------------------------------
# INICIALIZACE
# ---------------------------------

krupka = House()

# zapnutí 12V větve na zdroji
V12V.on()
V05V.on()


def RS485(krupka):
    ''' funkce pro kompletní obsluhu komunikace a ovládání terminálů  
        - funkce běží v threadu s WebControlem na pozadí
        
        
    '''
    
    def RS485(data):
        ''' odeslání řetězce do sériového portu
            řetězec bude konvertován na byte

            return:
                vrací případnou odpověď od dotazovaného terminálu

        '''
        # RS485 send data
        dir.on()
        sleep(0.01)

        data += "\r\n"
        
        buff = str.encode(data)

        # odeslání dat do sériového portu
        with serial.Serial(ser_port, ser_baud) as ser:
            ser.write(buff)

            # nutno počkat na odeslání
            # pro 57600 vychází 1byte (8bitů + 0bit parity + 1stop bit ) na cca 9× 17,4µs = 0,157ms
            d = round(len(buff)/5,0) + 1
            sleep(d/1000)

        # RS485 receive data    
        dir.off()    

        with serial.Serial(ser_port, ser_baud, timeout=0.2) as ser:
            line = ser.readline()
            
        try: # občas to odešle bláboly neslučitelné s UTF8
            buff = line.decode('utf-8').strip()
        except:
            buff = ""

        return buff

    def TimeAndDate():
        ''' odeslání časové značky do broadcastu sběrnice  

            každou hodinu si sáhne na NTP server
        '''

        global NTP_flag
        global NTP_hour

        # zjištění aktuálního času 
        dt = datetime.now()

        if (NTP_flag == False) or (NTP_hour < int(dt.strftime('%H'))):

            buff = ""
            NTP_hour = int(dt.strftime('%H'))

            try:
                # resp je struktura vyčtená z NTP packetu
                # resp.tx_time je počet sekund od Epochy (1.1.1970)
                resp = ntplib.NTPClient().request(NTP_sever1)  

                buff = datetime.fromtimestamp(resp.tx_time).strftime('%Y-%m-%d %H:%M:%S')

            except:
                print("NTP1 nedostupné...")

                try:
                    # resp je struktura vyčtená z NTP packetu
                    # resp.tx_time je počet sekund od Epochy (1.1.1970)
                    resp = ntplib.NTPClient().request(NTP_sever2)  

                    buff = datetime.fromtimestamp(resp.tx_time).strftime('%Y-%m-%d %H:%M:%S')

                except:
                    print("NTP2 nedostupné...")

                    try:
                        # resp je struktura vyčtená z NTP packetu
                        # resp.tx_time je počet sekund od Epochy (1.1.1970)
                        resp = ntplib.NTPClient().request(NTP_sever3)  

                        buff = datetime.fromtimestamp(resp.tx_time).strftime('%Y-%m-%d %H:%M:%S')

                    except:
                        print("ani NTP3 neni dostupné...")

            if len(buff):
                # nastavení aktuálně vyčteného času do systému RPi
                NTP_flag = True

                print(buff)

                try:
                    system("sudo date --set '{}'".format(buff))
                except:
                    print("Nepovedlo se nastavit čas")
                    NTP_flag = False

            dt = datetime.now()


        # sestavení zprávy s časovým razítkem
        buff = "ATiBT" + dt.strftime("%d.%m.%y %H:%M")

        # odeslání do sběrnice
        RS485(buff)

    def Term_State(addr):
        ''' odeslání dotazu na přítomnost terminálu

            return:

                - True: terminál komunikuje            
                - False: terminál je v chybovém stavu  

        '''

        buff = "ATi{}?".format(addr)
        err  = "ATa{}:OK".format(addr)

        # očekávám odpověď ve tvaru ATa<addr>:OK
        # odeslání do RS485
        answ = RS485(buff)

        if len(answ):
            if answ == err:
                return True
            else:
                return False
        else: 
            return False    
    
    def Term_Actual_Temp(addr):
        ''' odeslání dotazu na aktuální teplotu

            return:

                254 - 1/10 °C
                -1  - chybový stav
        '''
        
        buff = "ATi{}A".format(addr)
        
        # očekávám odpověď ve tvaru ATa<addr>:125
        # odeslání do RS485
        answ = RS485(buff)
        
        patt = re.compile('ATa(\d):(\d.+)')
        
        s = patt.match(answ)
        
        # chybná přijatá zpráva z RS485
        if s == None:
            return -1
        
        # kontrola adresy terminálu
        if int(s.group(1)) != addr:
            return -1
        
        return int(s.group(2))

    def Term_Actual_Humidity(addr):
        ''' odeslání dotazu na aktuální vlhkost

            return:

                456 - 1/10 % vlhkosti
                -1  - chybový stav
        '''
        
        buff = "ATi{}V".format(addr)
        
        # očekávám odpověď ve tvaru ATa<addr>:456
        # odeslání do RS485
        answ = RS485(buff)
        
        patt = re.compile('ATa(\d):(\d.+)')
        
        s = patt.match(answ)
        
        # chybná přijatá zpráva z RS485
        if s == None:
            return -1
        
        # kontrola adresy terminálu
        if int(s.group(1)) != addr:
            return -1
        
        return int(s.group(2))
   
    def Term_ByPass(addr):
        ''' odeslání dotazu na aktuální bypass

            return:
                (ByPass čas (min), ByPass teplota(1/10°C))
                -1  - chybový stav
        '''
        
        buff = "ATi{}B".format(addr)
        
        # očekávám odpověď ve tvaru ATa<addr>:120 275
        #                           ATa<addr>:None
        # odeslání do RS485
        answ = RS485(buff)
        
        patt = re.compile('ATa(\d):(\d.+) (\d.+)')
        
        s = patt.match(answ)
        
        # chybná přijatá zpráva z RS485
        if s == None:
            return (-1,-1)
        
        # kontrola adresy terminálu
        if int(s.group(1)) != addr:
            return (-1,-1)
        
        # bypass čas
        cas = int(s.group(2))
        tepl = int(s.group(3))
        
        # potvrzení přijatých dat do terminálu (ACK)
        # očekávám odpověď ve tvaru ATa<addr>:OK
        buff = "ATi{}P{} {}".format(addr,cas,tepl)
        answ = RS485(buff)
        
        patt = re.compile('ATa(\d):BPS-OK')
        s = patt.match(answ)
        
        if s == None:
            return (-1,-1)
        
        return (cas,tepl)
    
    def Term_Req(addr,t):
        ''' odeslání zjištěné požadované teploty

            return:
                 0 - komunikace OK
                -1 - chyba komunikace
        '''
        buff = "ATi{}Q{}".format(addr,t)
        
        # očekávám odpověď ve tvaru ATa<addr>:Q-OK271
        # odeslání do RS485
        answ = RS485(buff)
        
        patt = re.compile('ATa(\d):Q-OK(\d.+)')
        
        s = patt.match(answ)
        
        # chybná přijatá zpráva z RS485
        if s == None:
            return -1
        
        # kontrola adresy terminálu
        if int(s.group(1)) != addr:
            return -1
        
        # kontrola odeslané hodnoty
        if int(s.group(2)) != t:
            return -1        
        
        return 0
    
    def Term_Radiator(addr,t):
        ''' odeslání stavu radiátoru (topí | netopí)

            return:
                 0 - komunikace OK
                -1 - chyba komunikace
        '''
        if t == True:
            buff = "ATi{}R1".format(addr)
            err = 1
        else:
            buff = "ATi{}R0".format(addr)
            err = 0
        
        # očekávám odpověď ve tvaru ATa<addr>:R-OK1
        # odeslání do RS485
        answ = RS485(buff)
        
        patt = re.compile('ATa(\d):R-OK(\d)')
        
        s = patt.match(answ)
        
        # chybná přijatá zpráva z RS485
        if s == None:
            return -1
        
        # kontrola adresy terminálu
        if int(s.group(1)) != addr:
            return -1
        
        # kontrola odeslané hodnoty
        if int(s.group(2)) != err:
            return -1        
        
        return 0    
        
    def Term_bpsTime(addr,t):
        ''' odeslání zbývajícího času bypass režimu (v min.)

            return:
                 0 - komunikace OK
                -1 - chyba komunikace
        '''
        buff = "ATi{}D{}".format(addr,t)
        
        # očekávám odpověď ve tvaru ATa<addr>:D-OK65
        # odeslání do RS485
        answ = RS485(buff)
        
        patt = re.compile('ATa(\d):D-OK(\d.+)')
        
        s = patt.match(answ)
        
        # chybná přijatá zpráva z RS485
        if s == None:
            return -1
        
        # kontrola adresy terminálu
        if int(s.group(1)) != addr:
            return -1
        
        # kontrola odeslané hodnoty
        if int(s.group(2)) != t:
            return -1        
        
        return 0

                    
    while True:
        
      
        # pro každý terminál v domě nastavuji parametr oživení
        for i in krupka.dum:
            
            # if i.addr != 1:
            #     continue
        
            # čtyři opakované dotazy na přítomnost terminálu na sběrnici
            for x in range(4):
            
                if Term_State(i.addr):
                    i.alive = True
                    i.aliveDelay = 5
                    break
                else:
                    if i.aliveDelay == 0:
                        i.alive = False
                    else:
                        i.aliveDelay -= 1
                    continue
            
            if i.alive == False:
                i.ventil = False
                continue
            
            # všem posílám časové razítko
            TimeAndDate()
            
            # vyčtení aktuální teploty a vlhkosti
            buff = Term_Actual_Temp(i.addr)
            if buff > 0:
                i.tempAct = buff
                
            buff = Term_Actual_Humidity(i.addr)
            if buff > 0:
                i.humidity = buff

            # vyčtení požadavku na bypass
            buff = Term_ByPass(i.addr)
            
            if (buff[0] != -1):
                # bypass čas je ukládán jako konečná značka v minutách od počátku epochy
                i.bypass = int(datetime.timestamp(datetime.now()))//60 + buff[0]
                i.bpsTemp = buff[1]
                
            # zjištění a odeslání požadované teploty
            epo = int(datetime.timestamp(datetime.now()))//60
            
            if krupka.dovolena == True:   # nejvyšší prioritu má režim dovolená
                i.tempReq = i.tempDef
            else:
                if (i.bypass - epo) > 0:  # po ní následuje ByPass režim
                    i.tempReq = i.bpsTemp
                else:                     
                    i.bypass = 0          # není-li ByPass používán, je třeba jej udržovat na nule
                
                    buff = krupka.Pozadovana(i.addr)
                    if buff == -1:
                        i.tempReq = i.tempDef
                    else:
                        i.tempReq = buff
                    
            err = Term_Req(i.addr,i.tempReq)
            
            if err == -1:    # opakovaný pokus
                Term_Req(i.addr,i.tempReq)
        
            # odeslání status triaků
            if i.ventil == False:  # ventil uzavřen -> netopí se
                if i.tempAct > (i.tempReq - krupka.HYSTEREZE):
                    i.ventil = False
                else:
                    i.ventil = True    
                
            
            if i.ventil == True:   # ventil otevřen -> topí se
                if  (i.tempAct) < i.tempReq:
                    i.ventil = True
                else: 
                    i.ventil = False

            err = Term_Radiator(i.addr,i.ventil)
            
            if err == -1:    # opakovaný pokus
                err = Term_Radiator(i.addr,i.ventil)
            
            # odeslání zbývajícího času bypass
            epo = int(datetime.timestamp(datetime.now()))//60
            
            t = i.bypass - epo
            
            if  t < 0:
                t = 0
            
            err = Term_bpsTime(i.addr,t)
            
            if err == -1:   # opakovaný pokus
                err = Term_bpsTime(i.addr,t)
            
            
            # promítnutí stavu ventilu na výstupy RPi
            if i.ventil == True:
                triak[i.addr].on()
            else:
                triak[i.addr].off()
                
            # ovládání relé kotle
            # kotel je vypnut
            if krupka.kotel == 0:
                
                err = 0
                for v in krupka.dum:
                    if v.ventil == True:
                        err += 1
                # alespoň jeden ventil je otevřen
                if err > 0:
                    krupka.kotel = int(datetime.timestamp(datetime.now()))
            
            # kotel je vypnut, ale první ventil je otevřen 
            #   => běží odpočet zpoždění
            if krupka.kotel > 100:
                if int(datetime.timestamp(datetime.now())) - krupka.kotel > 60:
                    krupka.kotel = 1
                
            # kotel je sepnut
            if krupka.kotel == 1:
                
                err = 0
                for v in krupka.dum:
                    if v.ventil == True:
                        err += 1
                # všechny ventily jsou uzavřeny
                if err == 0:
                    krupka.kotel = 0
            
            # promítnutí do výstupů na RPi
            if krupka.kotel == 1:
                RELE1.on()
            elif krupka.kotel == 0:
                RELE1.off()
            
        
            # oddych procesoru
            sleep(0.15)

       

# zapouzdření instance třídy Flask do funkce 
# pro spuštění ve vlákně na pozadí
def threadWeb():
    WebServer = Flask(__name__)
    
    # ---------------------------------
    # Routování
    # ---------------------------------

    # stránka indexu
    #   - do šablony posílám celý seznam s objekty jednotlivých místností
    #     a aktuální stav parametru dovolená
    @WebServer.route("/", methods = ["GET", "POST"])
    def index():

        bps_form = request.form

        # test příjmu dat z formuláře
        if len(bps_form):
            
            if bps_form['dov_set'] == "ON":
                krupka.dovolena = True
                return render_template("index.html",dum=krupka.dum, dov=krupka.dovolena)
                
                            
            if bps_form['dov_set'] == "OFF":
                krupka.dovolena = False
                return render_template("index.html",dum=krupka.dum, dov=krupka.dovolena)

                
            for index, room in enumerate(krupka.dum):
                if room.addr == int(bps_form['address']):
                    i = index
                    break

            krupka.dum[i].Set_bpsTemp(bps_form['bps_temp'])
            krupka.dum[i].Set_bypass(bps_form['bps_time'])

        return render_template("index.html",dum=krupka.dum, dov=krupka.dovolena)


    # stránky s nastavením bypassu pro jednotlivé místnosti
    #   - v index.html jsou generovány odkazy podle adresace na RS sběrnici
    #     a to je nutno převést na příslušný index v seznamu místností
    #   - pak se podle šablony vygeneruje stránka pro ByPass nastavení
    #   - do šablony posílám jen příslušnou položku seznamu podle dohledaného indexu
    @WebServer.route("/bypass/<int:room_addr>/", methods = ["GET", "POST"])
    def bypass(room_addr):
        for index, room in enumerate(krupka.dum):
            if room.addr == room_addr:
                i = index
                break
        return render_template("bypass.html",room=krupka.dum[i])
    
    
    # stránky s nastavením parametrů systému
    #  - pojmenování místností
    #  - přidělení adresace 
    #  - nastavení defaultní teploty každé místnosti
    # ve smyčce jsou dvě stránky, jedna pouze zobrazující nastavené hodnoty a druhá pro jejich editaci
    @WebServer.route("/view_nast", methods = ["GET", "POST"])
    def view_nast():
        
        s_form = request.form
        
        # test příjmu dat z formuláře
        if len(s_form):
            
            # adresa editované místnosti
            a = int(s_form['id'])
            
            # hledám index v seznamu
            i=None
            for index, room in enumerate(krupka.dum):
                if room.addr == a:
                    i = index
                    break            
            
            if i==None:
                print("Divná chyba, index místnosti nenalezen")
                return render_template("view_nast.html",dum=krupka.dum)
            
            # nastavení parametrů z formuláře
            krupka.dum[i].room = s_form['pojmenovani']
            
            try:
                x = int(s_form['adresace'])
                
                if x>0 or x<7:
                    krupka.dum[i].addr = x
            except ValueError:
                pass
            
            print("Zkouším nastavit defaultní teplotu: {}".format(s_form['teplota']))
            
            krupka.dum[i].Set_tempDef(s_form['teplota'])

        return render_template("view_nast.html",dum=krupka.dum)

    @WebServer.route("/room/<int:room_addr>")
    def nastaveni(room_addr):
        for index, room in enumerate(krupka.dum):
            if room.addr == room_addr:
                i = index
                break
        
        return render_template("nastaveni.html",room=krupka.dum[i])

    # Stránky s nastavením intervalů
    # ve smyčce jsou dvě stránky, jedna pouze zobrazující nastavené hodnoty a druhá pro jejich editaci
    @WebServer.route("/view_int", methods = ["GET", "POST"])
    def view_int():
        
        s_form = request.form
        
        # flag 0=nic nezobrazt
        #      1=zobrazit tlačítko k uložení změn
        #      2=zobrazit hlášení o korektním uložení
        flag = 0
        
      
        # test příjmu dat z formuláře
        if len(s_form):
            
            # test na příkaz k uložení záznamů
            try:
                # pokyn k uložení změněných záznamů
                if s_form['save'] != None:
                    flag = 2
                    
                    print("Pokus o uložení intervalů...")
                    
                    
                    krupka.UlozitIntervaly()
                    
                
                    return render_template("view_int.html",interval=krupka.interval,mistn=krupka.mistn,fl_save=flag)
            except:
                pass
            
            # test na smazání záznamu
            try:
                if s_form['ID_del'] != None:
                    err = 0
            except:
                err = 1
                
            if err == 0:        
                flag = 1
                
                index = int(s_form['ID_del'])
                
                # odstranění položky ze seznamu
                krupka.interval.pop(index)
                
                # nutno přeindexovat celý seznam
                index=0
                
                for b in krupka.interval:
                    b.id = index
                    index +=1
                
                return render_template("view_int.html",interval=krupka.interval,mistn=krupka.mistn,fl_save=flag)
                
            
            flag = 1
            
            # vyčtení ID intervalu
            ID = int(s_form['ID'])
            
            
            #vyčtení seznamu checkboxíků s adresami místností
            r = s_form.getlist('rooms')
            
            # vyčtení seznamu checkboxíků se zvolenými dny
            d = s_form.getlist('days')
            
            # debug výpis          
            print("{} místnosti\n".format(len(r)))
            print(r)    
            print("{} dny\n".format(len(d)))
            print(d)
            
            krupka.interval[ID].SetRooms(r)
            krupka.interval[ID].SetDays(d)
            
            # počátek intervalu (_OD_)
            o = s_form['od']
            krupka.interval[ID].SetOd(o)
            
            # konec intervalu (_DO_)
            d = s_form['do']
            krupka.interval[ID].SetDo(d)
            
            # požadovaná teplota
            t = s_form['temp']
            krupka.interval[ID].SetTemp(t)
            
            
        return render_template("view_int.html",interval=krupka.interval,mistn=krupka.mistn,fl_save=flag)

    @WebServer.route("/interval/<int:int_id>")
    def interval(int_id):
        
        index = int_id
        
        # ID 1000 je pro nový interval
        if index == 1000:
            index = krupka.AddNewInt()
            
        print(str(index))
        
        return render_template("intervaly.html",id=index, interval=krupka.interval[index], mistn=krupka.mistn)

    @WebServer.route("/delint/<int:int_id>")
    def delint(int_id):
        
        index = int_id
        
        
        return render_template("delete.html",id=index, interval=krupka.interval[index], mistn=krupka.mistn)

    # Stránka s ovládáním napájení terminálů
    @WebServer.route("/napajeni", methods = ["GET", "POST"])
    def napajeni():
        
        s_form = request.form
        
        # test příjmu dat z formuláře
        if len(s_form):
            
            if s_form['nap_set'] == "ON":
                V12V.on()
                krupka.napajeni = True

            if s_form['nap_set'] == "OFF":
                V12V.off()
                krupka.napajeni = False
                        
        return render_template("napajeni.html", nap=krupka.napajeni)
    


    # spuštění web serveru
    WebServer.run(debug=True, use_reloader=False, host="0.0.0.0", port="80")
    

# ---------------------------------
# spuštění html serveru
# ---------------------------------

# spuštění instance třídy Flask / aplikace
if __name__ == "__main__":
    
    t_WebApp = threading.Thread(name='Flask AntiqueCZ', target=threadWeb)
    t_WebApp.setDaemon(True)
    t_WebApp.start()
    
    try:
        RS485(krupka)
        
            
    except KeyboardInterrupt:
        print("končím...")
        exit(0)
    
    
    