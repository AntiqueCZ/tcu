# -*- coding: utf-8 -*-

import pytest
from house import House
from intervaly import Interval

def test_Pozadovana():
    
    h = House()
    i1 = Interval()
    i2 = Interval()    
    
    # nutno smazat načtený seznam intevalů pro korektní výsledky
    h.interval = []
    
    i1.SetOd("8:00")
    i1.SetDo("12:35")
    i1.SetDays({3,4,5})
    i1.SetRooms({1,2})
    i1.SetTemp("25,5")
    i1.id = 0
    
    h.interval.append(i1)
    
    i2.SetOd("17:00")
    i2.SetDo("23:35")
    i2.SetDays({6,7})
    i2.SetRooms({1,5,6})
    i2.SetTemp("18,3")
    i2.id = 1
    
    h.interval.append(i2)
    
    err = h.Pozadovana(6,debug=True,cas=1100,den=6)
    assert err == 183
    
    err = h.Pozadovana(6,debug=True,cas=1100,den=5)
    assert err == -1

    err = h.Pozadovana(2,debug=True,cas=550,den=3)
    assert err == 255
    
    err = h.Pozadovana(3,debug=True,cas=550,den=3)
    assert err == -1
    
    err = h.Pozadovana(2,debug=True,cas=550,den=7)
    assert err == -1
    
    err = h.Pozadovana(2,debug=True,cas=840,den=3)
    assert err == -1
