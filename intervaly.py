class Interval:
    ''' třída pro data intervalů
    id     - ID intervalu (počítáno od nuly)
    od     - minuty (možno zadat v hh:mm)
    do     - minuty (možno zadat v hh:mm)
    rooms  - množina číselných (ID=RS adresa) označení pokojů  
    days   - množina číselných označení dnů v týdnu
    temp   - požadovaná teplota (275 => 27,5°C)  

    '''
    def  __init__(self):
        self.id = None
        self.od = None
        self.do = None
        self.rooms = set()
        self.days = set()
        self.temp = 0

    def SetOd(self,s_od):
        ''' nastavení parametru Od
        
        údaj ukládán v minutách od počátku dne (max tedy 24×60 = 1440 min)
        
        return 0 - OK
               1 - Error
        '''
        # očištění vstupu od bílého balastu
        buff = s_od.strip()

        # kontrola délky řetězce
        # očekávám formát hh:mm nebo h:mm
        if len(buff) != 5 and len(buff) != 4:
            print('Nesprávná délka vstupního řetězce')
            return 1

        try:
            if len(buff) == 5:
                hod = int(buff[:2])
            else:
                hod = int(buff[:1])
            
        except ValueError:
            print("Chyba procesu: nečíselné zadání času")
            self.od = None
            return 2
    
        try:
            min = int(buff[-2:])
            
        except ValueError:
            print("Chyba procesu: nečíselné zadání času")
            self.od = None
            return 3

        self.od = 60 * hod + min

        return 0

    def SetDo(self,s_do):
        ''' nastavení parametru Do
        
        údaj ukládán v minutách od počátku dne (max tedy 24×60 = 1440 min)
        
        return 0 - OK
               1 - Nesprávná délka řetězce 
               2 - nečíselné zadání hod
               3 - nečíselné zadání min
               4 - Do je menší nebo rovno Od
        '''
        # očištění vstupu od bílého balastu
        buff = s_do.strip()

        # kontrola délky řetězce
        # očekávám formát hh:mm
        if len(buff) != 5 and len(buff) != 4:
            print('Nesprávná délka vstupního řetězce')
            return 1

        try:
            if len(buff) == 5:
                hod = int(buff[:2])
            else:
                hod = int(buff[:1])
            
        except ValueError:
            print("Chyba procesu: nečíselné zadání času")
            self.do = None
            return 2
    
        try:
            min = int(buff[-2:])
            
        except ValueError:
            print("Chyba procesu: nečíselné zadání času")
            self.do = None
            return 3

        self.do = 60 * hod + min

        if self.od is not None:
            if self.do <= self.od:
                print("Chyba procesu: hodnota Do je menší, nebo rovna hodnotě Od")
                self.do = None
                return 4

        return 0
            
    def SetRooms(self,l_rooms):
        ''' nastavení parametru Rooms
        
        s_rooms - získané pole z webového rozhraní (checkboxy)
        
        return 0 - OK
               1 - žádná místnost
        '''

        # vyprázdnění množiny
        self.rooms.clear()
        
        # akceptuji i prázdnou množinu
        if len(l_rooms) == 0:
            return 1

        # projdu postupně všechny znaky vstupního řetězce
        for i in l_rooms:

            self.rooms.add(int(i))

        return 0
            
    def SetDays(self,l_days):
        ''' nastavení parametru Days (list of int) 
        
        s_days - získané pole (class 'list') z webového rozhraní
               
        days   - množina všech dní  = {1,2,3,4,5,6,7,8}
                 1 pondělí
                 2 úterý
                 3 středa
                 4 čtvrtek
                 5 pátek
                 6 sobota
                 7 neděle
                 8 svátky
        
        return 0 - OK
               1 - žádný den
        '''

        # vyprázdnění množiny
        self.days.clear()
        
        # akceptuji i prázdnou množinu
        if len(l_days) == 0:
            return 1

        # projdu postupně všechny položky pole
        # protože je to pole z checkboxů, není potřeba dalších kontrol validity dat
        for i in l_days:
            self.days.add(int(i))

        return 0

    def View(self,v="",r=dict()):
        ''' zobrazení hodnot pro html zobrazení
        '''
        
        if v=="od":
            # // je celočíselné dělení
            # %  je zbytek po dělění
            if self.od == None:
                return "None"

            return "{:0>2}:{:0>2}".format(self.od//60,self.od%60)
        
        elif v=="do":
            # // je celočíselné dělení
            # %  je zbytek po dělění
            if self.do == None:
                return "None"

            return "{:0>2}:{:0>2}".format(self.do//60,self.do%60)
        
        elif v=="days":
            buff = ""
            buff2= "" 
            
            if 1 in self.days:
                buff = "po"
            
            if 2 in self.days:
                if len(buff)>0:
                    buff += ", út"
                else:
                    buff = "út"
            if 3 in self.days:
                if len(buff)>0:
                    buff += ", st"
                else:
                    buff = "st"
            if 4 in self.days:
                if len(buff)>0:
                    buff += ", čt"
                else:
                    buff = "čt"
            if 5 in self.days:
                if len(buff)>0:
                    buff += ", pá"
                else:
                    buff = "pá"  
            if 6 in self.days:
                if len(buff2)>0:
                    buff2 += ", so"
                else:
                    buff2 = "so"
            if 7 in self.days:
                if len(buff2)>0:
                    buff2 += ", ne"
                else:
                    buff2 = "ne"
            if 8 in self.days:
                if len(buff2)>0:
                    buff2 += ", sv"
                else:
                    buff2 = "sv"  
            
            if len(buff) == 0:
                return buff2
            
            else:
                return buff + "<br />" + buff2
            
        elif v=="rooms":
            buff=""
            
            for i in self.rooms:
                buff += r[i] + "<br />\n"
                
            return buff
        
        elif v=="temp":
            return "{:.1f}°C".format(self.temp/10)
        
        else:
            return "err"
        
    def Edit(self,v=""):
        ''' zobrazení hodnot pro html editaci
        '''
        if v=="od":
            return "{:0>2}:{:0>2}".format(self.od//60,self.od%60)

        elif v=="do":
            return "{:0>2}:{:0>2}".format(self.do//60,self.do%60)
        
        elif v=="temp":
            return "{:.1f}".format(self.temp/10)
        
    def SetTemp(self,s_temp):
        ''' převod řetězce na teplotu intervalu (23,5 | 28.7)
        
        return:
            0 - OK
            1 - prázdný řetězec, nastavuji 18°C
            2 - teplota mimo rozsah, beze změny
            3 - nečíselné zadání (ValueError), opět beze změny
        '''
        
        buff = s_temp.strip()
        
        # prázdný řetězec - nastavuji 18°C
        if len(buff) == 0:
            self.temp = 180
            return 1

        try:
            # náhrada čárky za tečku -> převod na float a vynásobení 10 -> převod na int
            x = int(float(buff.replace(',','.'))*10)

            if 100 < x < 350:
                self.temp = x
                return 0
            else:
                print("Chyba: požadovaná teplota je mimo povolený rozsah")
                return 2
            
        except ValueError:
            print("Chyba: nečíselné zadání teploty")
            return 3
        
        